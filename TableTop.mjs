"use strict";

import BaseComponent from '/base-component/BaseComponent.mjs'

export default class TableTop extends BaseComponent {

  /**
   * If the component needs to dynamic create the style with
   * Some constructor or another context, set style as a method
   * instead of an string attribute.
   */
  style = `
    #table-top {
      background: var(--table-bg);
      border: 2px solid #CCC;
      border-radius: 8px;
      padding: 10px;
    }
  `

  template = `
    div#table-top
      p
        @text Isso é um teste.
  `

  constructor() {
    super()
  }

}

TableTop.style
